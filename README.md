# Bamboo Master in Docker

This is still a bit experimental, but basically creates a lightweight image that allows
us to build a replaceable container running Bamboo.

Under the utils directory are some commands that help with building/rebuilding this image.

Typical example of running the container:

```
docker run -d \
  -e BAMBOO_VERSION=6.3.0 \
  -p 8085:8085 \
  -p 54663:54663 \
  -v /mnt/bamboo-efs/bamboo-home:/opt/bamboo/bamboo-home \
  -v /mnt/bamboo-efs/bamboo-tomcat-logs:/opt/bamboo/atlassian-bamboo-6.3.0/logs \
  --name bamboo-server \
  robotlimeltd/bamboo-server:1.1 \
```
