FROM openjdk:8

# Environment
ENV BAMBOO_USER bamboo
ENV BAMBOO_ROOT_DIR /opt/bamboo
ENV BAMBOO_HOME /opt/bamboo/bamboo-home
ENV BAMBOO_SRC https://www.atlassian.com/software/bamboo/downloads/binary
ENV BAMBOO_VERSION 6.3.0
ENV BAMBOO_INSTALLER /tmp/init-bamboo.sh

# Expose web and agent ports
EXPOSE 8085
EXPOSE 54663

# Set up basic Bamboo Environment
RUN mkdir -p $BAMBOO_HOME \
    && mkdir -p $BAMBOO_ROOT_DIR/atlassian-bamboo/logs \
    && groupadd --gid 6666 $BAMBOO_USER \
    && useradd --uid 6666 -g $BAMBOO_USER -d $BAMBOO_HOME -s /sbin/nologin -c "Bamboo User" $BAMBOO_USER \
    && chown -R $BAMBOO_USER $BAMBOO_ROOT_DIR \
    && chgrp -R $BAMBOO_USER $BAMBOO_ROOT_DIR

# Set up installer
COPY ./files/init-bamboo-sh $BAMBOO_INSTALLER
RUN chown $BAMBOO_USER $BAMBOO_INSTALLER \
    && chmod 750 $BAMBOO_INSTALLER

# Execute installer while in main bamboo installation directory
# as the user "bamboo"
WORKDIR $BAMBOO_ROOT_DIR
USER $BAMBOO_USER
CMD /tmp/init-bamboo.sh

